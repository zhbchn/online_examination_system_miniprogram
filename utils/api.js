const ApiRootUrl = "http://localhost:8360/";

module.exports = {
	userInfor: ApiRootUrl + "user/user", //登录查询用户信息接口
	bind: ApiRootUrl + "user/bind", //绑定用户信息接口
	professionUrl: ApiRootUrl + "user/getProfession", //获取专业数据接口

	questions: ApiRootUrl + "question/getQuestion", //试题信息接口
	options: ApiRootUrl + "question/getOptions", //选项信息接口

	paper: ApiRootUrl + "paper/getPaperList", //考试列表信息接口
	againTestUrl: ApiRootUrl + "paper/againTest", //检查重复考试接口

	modify: ApiRootUrl + "modify/modify", //修改答题接口

	markUrl: ApiRootUrl + "mark/getMarkList", //获取分数接口

	getDetailListUrl: ApiRootUrl + "detail/getDetailList" //获取详细的答题记录
};
