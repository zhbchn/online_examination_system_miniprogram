// pages/mark/mark.js
const api = require("../../utils/api.js");
const util = require("../../utils/util.js");
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    no: "",
    name: "",
    markList: [] //各科分数列表
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let that = this;
    util
      .request(
        api.markUrl,
        {
          stu_no: app.globalData.no
        },
        "POST"
      )
      .then(function(res) {
        that.setData({
          no: app.globalData.no,
          name: app.globalData.name,
          markList: res.data.data.markList
        });
      });
  }
});
