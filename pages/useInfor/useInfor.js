// pages/useInfor/useInfor.js

var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    no: "",
    name: "",
    profession: "",
    phone: "",
    email: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */

  onLoad: function(options) {
    this.setData({
      no: app.globalData.no,
      name: app.globalData.name,
      profession: app.globalData.profession,
      phone: app.globalData.phone,
      email: app.globalData.email
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {}
});
