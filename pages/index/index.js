// pages/personal/index/index.js

const api = require("../../utils/api.js");
const util = require("../../utils/util.js");

var app = getApp();

Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		userInfo: "",
		hasUserInfo: false
	},

	/**
	 * 获取用户信息
	 */
	getUserInfo: function() {
		if (app.globalData.userInfo) {
			this.setData({
				userInfo: app.globalData.userInfo,
				hasUserInfo: true
			});
		} else if (this.data.canIUse) {
			// 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
			// 所以此处加入 callback 以防止这种情况
			app.userInfoReadyCallback = res => {
				this.setData({
					userInfo: res.userInfo,
					hasUserInfo: true
				});
			};
		} else {
			// 在没有 open-type=getUserInfo 版本的兼容处理
			wx.getUserInfo({
				success: res => {
					this.setData({
						userInfo: res.userInfo,
						hasUserInfo: true
					});
				}
			});
		}
	},

	/**
	 * 检查用户是否已经注册
	 * @openid 用户的openid
	 */
	isBind: function(openid) {
		util
			.request(
				api.userInfor, {
					openid: openid
				},
				"POST"
			)
			.then(function(res) {
				console.log(res);
				if (res.data.code === 101) {
					wx.showModal({
						content: "[未验证学号]，无法正常使用系统",
						showCancel: false,
						confirmText: "验证学号",
						success: function(res) {
							//跳转到绑定界面
							wx.navigateTo({
								url: "../bind/bind"
							});
						}
					});
				} else {
					app.globalData.name = res.data.data.userInfor.student_name;
					app.globalData.no = res.data.data.userInfor.student_no;
					app.globalData.profession = res.data.data.userInfor.profession_name;
					app.globalData.email = res.data.data.userInfor.student_email;
					app.globalData.phone = res.data.data.userInfor.student_phone;
				}
			});
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function() {
		var that = this;
		that.getUserInfo();
		util.getOpenId().then(function(res) {
			app.globalData.openid = res;
			that.isBind(res);
		});
	}
});
