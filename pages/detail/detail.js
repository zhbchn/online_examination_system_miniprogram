// pages/detail/detail.js

const api = require("../../utils/api.js");
const util = require("../../utils/util.js");
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    index: 0,
    detail: "",
    detailList: ""
  },

  changetQuestion: function(event) {
    this.setData({
      index: event.currentTarget.dataset.index
    });
    this.setData({
      detail: this.data.detailList[this.data.index]
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let that = this;
    util
      .request(
        api.getDetailListUrl,
        {
          stu_no: app.globalData.no,
          profession_no: that.options.profession_no,
          course_no: that.options.course_no
        },
        "POST"
      )
      .then(function(res) {
        that.setData({
          detailList: res.data.data.detailList,
          detail: res.data.data.detailList[that.data.index]
        });
      });
  }
});
